package com.faizan.moviesapp.data.db.entity

import androidx.room.Entity
import androidx.room.Index
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName

@Entity(tableName = "similar_movies", indices = [Index(value = ["id"], unique = true)])

data class SimilarMoviesEntry(
    val adult: Boolean,
    @SerializedName("backdrop_path")
    val backdropPath: String? = null,
//    @SerializedName("genre_ids")
//    val genreIds: List<Int>? = null,
    @PrimaryKey(autoGenerate = false)
    val id: Int? = null,
    @SerializedName("original_language")
    val originalLanguage: String,
    @SerializedName("original_title")
    val originalTitle: String,
    val overview: String,
    val popularity: Double,
    @SerializedName("poster_path")
    val posterPath: String? = null,
    @SerializedName("release_date")
    val releaseDate: String,
    val title: String,
    val video: Boolean,
    @SerializedName("vote_average")
    val voteAverage: Double,
    @SerializedName("vote_count")
    val voteCount: Int
)