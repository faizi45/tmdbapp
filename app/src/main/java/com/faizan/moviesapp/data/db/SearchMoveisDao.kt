package com.faizan.moviesapp.data.db

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.faizan.moviesapp.data.network.response.SearchMovieEntry

@Dao
interface SearchMoveisDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun upsert(searchMovieEntry: List<SearchMovieEntry>)

    @Query("Select * from popular_movies")
    fun getSearchedMovies(): LiveData<List<SearchMovieEntry>>
}