package com.faizan.moviesapp.data.db

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.faizan.moviesapp.data.db.entity.SimilarMoviesEntry

@Dao
interface SimilarMoviesDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun upsert(similarMoviesEntry: List<SimilarMoviesEntry>)

    @Query("Select * from similar_movies")
    fun getSimilarMovies(): LiveData<List<SimilarMoviesEntry>>
}