package com.faizan.moviesapp.data.db

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.faizan.moviesapp.data.db.entity.MovieDetailResponseDto

@Dao
interface MovieDetailsDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun upsert(movieDetailResponseDto: MovieDetailResponseDto)

    @Query("Select * from movie_details where id = :movieId")
    fun getMovieDetails(movieId: Int): LiveData<MovieDetailResponseDto>
}