package com.faizan.moviesapp.data.repository

import androidx.lifecycle.LiveData
import com.faizan.moviesapp.data.db.entity.*
import com.faizan.moviesapp.data.network.response.SearchMoviesResponse

interface TMDBRepository {

    suspend fun getPopularMovies(): LiveData<out List<PopularMoviesEntry>>

    suspend fun getPopularTvShows(): LiveData<out List<PopularTvEntry>>

    suspend fun getMoviesGenresList(): LiveData<out List<GenreEntry>>

    suspend fun getSearchMoviesResponse(query: String): LiveData<SearchMoviesResponse>

    suspend fun getSimilarMovies(movieId: Int): LiveData<out List<SimilarMoviesEntry>>
    suspend fun getMovieDetailsResponse(movieId: Int): LiveData<out MovieDetailResponseDto>
}