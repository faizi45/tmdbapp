package com.faizan.moviesapp.data.db.entity

import androidx.room.Entity
import androidx.room.Index
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName

@Entity(tableName = "movie_details", indices = [Index(value = ["id"], unique = true)])

data class MovieDetailResponseDto(
    val adult: Boolean,
    @SerializedName("backdrop_path")
    val backdropPath: String? = null,
    val budget: Int,
//    val genres: List<Genre>,
//    val homepage: Any,
    @PrimaryKey(autoGenerate = false)
    val id: Int,
    @SerializedName("imdb_id")
    val imdbId: String? = null,
    @SerializedName("original_language")
    val originalLanguage: String,
    @SerializedName("original_title")
    val originalTitle: String,
    val overview: String,
    val popularity: Double,
    @SerializedName("poster_path")
    val posterPath: String? = null,
//    @SerializedName("production_companies")
//    val productionCompanies: List<ProductionCompany>? = null,
    @SerializedName("release_date")
    val releaseDate: String,
    val revenue: Long,
    val runtime: Int,
//    @SerializedName("spoken_languages")
//    val spokenLanguages: List<SpokenLanguage>,
    val status: String,
    val tagline: String,
    val title: String,
    val video: Boolean,
    @SerializedName("vote_average")
    val voteAverage: Double,
    @SerializedName("vote_count")
    val voteCount: Int
)