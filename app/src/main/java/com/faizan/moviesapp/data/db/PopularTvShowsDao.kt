package com.faizan.moviesapp.data.db

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.faizan.moviesapp.data.db.entity.PopularTvEntry

@Dao
interface PopularTvShowsDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(popularTvEntry: List<PopularTvEntry>)

    @Query("Select * from popular_tv_shows")
    fun getPopularTvShows(): LiveData<List<PopularTvEntry>>
}