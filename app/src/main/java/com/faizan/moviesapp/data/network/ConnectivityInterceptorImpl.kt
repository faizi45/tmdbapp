package com.faizan.moviesapp.data.network

import android.content.Context
import android.net.ConnectivityManager
import com.faizan.moviesapp.internal.NoConnectivityException
import okhttp3.Interceptor
import okhttp3.Response

class ConnectivityInterceptorImpl(context: Context) :
    ConnectivityInterceptor {

    private val appContext = context.applicationContext

    override fun intercept(chain: Interceptor.Chain): Response {
        if (!isOnline())
            throw NoConnectivityException()
        else return chain.proceed(chain.request())

    }

    private fun isOnline(): Boolean {
        //we use this method to check if user is connected to network
        //and then fetch data from online or offline storage on this decision

        val connectivityManager = appContext.getSystemService(Context.CONNECTIVITY_SERVICE)
                as ConnectivityManager

        val networkInfo = connectivityManager.activeNetworkInfo

        return networkInfo != null && networkInfo.isConnected
    }
}