package com.faizan.moviesapp.data.db

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.faizan.moviesapp.data.db.entity.GenreEntry

@Dao
interface MoviesGenresDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun upsert(movieGenreEntry: List<GenreEntry>)

    @Query("Select * from movies_genres")
    fun getMoviesGenres(): LiveData<List<GenreEntry>>
}