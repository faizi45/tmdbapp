package com.faizan.moviesapp.data.network.response

import com.faizan.moviesapp.data.db.entity.PopularTvEntry
import com.google.gson.annotations.SerializedName

data class PopularTvShowsResponse(
    val page: Int,
    @SerializedName("results")
    val popularTvEntries: List<PopularTvEntry>,
    @SerializedName("total_pages")
    val totalPages: Int,
    @SerializedName("total_results")
    val totalResults: Int
)