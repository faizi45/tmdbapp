package com.faizan.moviesapp.data.network

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.faizan.moviesapp.data.db.entity.MovieDetailResponseDto
import com.faizan.moviesapp.data.network.response.*
import com.faizan.moviesapp.internal.NoConnectivityException

class MovieNetworkDataSourceImpl(
    private val tmdbApiService: TMDBApiService
) : MovieNetworkDataSource {

    private val _downloadedPopularMovies = MutableLiveData<Resource<PopularMoviesResponse>>()

    private val _downloadedPopularTvShows = MutableLiveData<PopularTvShowsResponse>()

    private val _downloadedSearchMovies = MutableLiveData<SearchMoviesResponse>()

    private val _downloadedMoviesGenres = MutableLiveData<MoviesGenresListResponse>()

    private val _downloadedMoviesDetails = MutableLiveData<MovieDetailResponseDto>()

    private val _downloadedSimilarMovies = MutableLiveData<SimilarMoviesResponse>()

    override val downloadedPopularMoviesResponse: LiveData<Resource<PopularMoviesResponse>>
        get() = _downloadedPopularMovies

    override val downloadedPopularTvShowsResponse: LiveData<PopularTvShowsResponse>
        get() = _downloadedPopularTvShows

    override val downloadedSearchMoviesResponse: LiveData<SearchMoviesResponse>
        get() = _downloadedSearchMovies

    override val downloadedMoviesGenres: LiveData<MoviesGenresListResponse>
        get() = _downloadedMoviesGenres

    override val downloadedMovieDetails: LiveData<MovieDetailResponseDto>
        get() = _downloadedMoviesDetails

    override val downloadedSimilarMoviesResponse: LiveData<SimilarMoviesResponse>
        get() = _downloadedSimilarMovies

    override suspend fun fetchPopularMovies(sortBy: String) {
        //method will fetch popular movies from api
        //and update livedata using mutable live data
        try {
            val fetchedPopularMovies = tmdbApiService.getPopularMovies(sortBy).await()
            _downloadedPopularMovies.postValue(Resource.success(fetchedPopularMovies.body()))
            if (fetchedPopularMovies.isSuccessful)
                Result.Success(
                    _downloadedPopularMovies.postValue(Resource.success(fetchedPopularMovies.body()))
                )
            else {
                Result.Error(Exception("sad"))
                Log.e("MOVIENETWORKDS", fetchedPopularMovies.isSuccessful.toString())
                Log.e("MOVIENETWORKDS", fetchedPopularMovies.message())


            }
            Log.e("MOVIENETWORKDS", fetchedPopularMovies.isSuccessful.toString())
            Log.e("MOVIENETWORKDS", fetchedPopularMovies.message())
            Log.e("MOVIENETWORKDS", fetchedPopularMovies.code().toString())
        } catch (e: Exception) {
            Log.e("MOVIENETWORKDS", "Error", e.fillInStackTrace())
        }

    }

    override suspend fun fetchPopularTvShows(sortBy: String) {

        try {
            val fetchedPopularTvShows = tmdbApiService.getPopularTvShows(sortBy).await()
            _downloadedPopularTvShows.postValue(fetchedPopularTvShows)
        } catch (e: NoConnectivityException) {
            Log.e("Connectivity", "No Internet", e)
        }
    }

    override suspend fun searchMovies(query: String) {

        try {
            val searchedMovies = tmdbApiService.searchMovie(query).await()
            _downloadedSearchMovies.postValue((searchedMovies))
        } catch (e: NoConnectivityException) {
            Log.e("Connectivity", "No Internet", e)
        }
    }


    override suspend fun fetchMoviesGenres() {
        try {
            val fetchedMoviesGenres = tmdbApiService.getMoviesGenres().await()
            _downloadedMoviesGenres.postValue(fetchedMoviesGenres)
        } catch (e: NoConnectivityException) {
            Log.e("Connectivity", "No Internet", e)
        }

    }

    override suspend fun fetchMovieDetails(id: Int) {

        try {
            val fetchedMoviesDetails = tmdbApiService.getMovieDetails(id).await()
            _downloadedMoviesDetails.postValue(fetchedMoviesDetails)
        } catch (e: NoConnectivityException) {
            Log.e("Connectivity", "No internet", e)
        }

    }

    override suspend fun fetchSimilarMovies(movieId: Int) {
        try {
            val fetchedSimilarMovies = tmdbApiService.getSimilarMovies(movieId).await()
            _downloadedSimilarMovies.postValue(fetchedSimilarMovies)
        } catch (e: NoConnectivityException) {
            Log.e("Connectivity", "No internet.", e)
        }

    }
}