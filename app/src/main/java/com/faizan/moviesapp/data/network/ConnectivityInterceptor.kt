package com.faizan.moviesapp.data.network

import okhttp3.Interceptor

interface ConnectivityInterceptor : Interceptor {
    //used for dependency injection via kodein
}