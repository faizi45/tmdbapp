package com.faizan.moviesapp.data.network

import androidx.lifecycle.LiveData
import com.faizan.moviesapp.data.db.entity.MovieDetailResponseDto
import com.faizan.moviesapp.data.network.response.*

interface MovieNetworkDataSource {

    val downloadedPopularMoviesResponse: LiveData<Resource<PopularMoviesResponse>>
    val downloadedPopularTvShowsResponse: LiveData<PopularTvShowsResponse>
    val downloadedSearchMoviesResponse: LiveData<SearchMoviesResponse>
    val downloadedMoviesGenres: LiveData<MoviesGenresListResponse>

    val downloadedMovieDetails: LiveData<MovieDetailResponseDto>

    val downloadedSimilarMoviesResponse: LiveData<SimilarMoviesResponse>

    suspend fun fetchPopularMovies(sortBy: String)

    suspend fun fetchPopularTvShows(sortBy: String)

    suspend fun searchMovies(query: String)

    suspend fun fetchMoviesGenres()

    suspend fun fetchSimilarMovies(movieId: Int)
    suspend fun fetchMovieDetails(id: Int)
}