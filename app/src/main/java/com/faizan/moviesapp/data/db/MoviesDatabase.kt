package com.faizan.moviesapp.data.db

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.faizan.moviesapp.data.db.entity.*
import com.faizan.moviesapp.data.network.response.SearchMovieEntry

@Database(
    entities = [PopularMoviesEntry::class, PopularTvEntry::class,
        SearchMovieEntry::class, GenreEntry::class, MovieDetailResponseDto::class, SimilarMoviesEntry::class],
    version = 1
)

abstract class MoviesDatabase : RoomDatabase() {

    abstract fun popularMoviesDao(): PopularMoviesDao
    abstract fun popularTvShowsDao(): PopularTvShowsDao
    abstract fun searchMoviesDao(): SearchMoveisDao

    abstract fun moviesGenresDao(): MoviesGenresDao

    abstract fun movieDetailsDao(): MovieDetailsDao

    abstract fun similarMoviesDao(): SimilarMoviesDao

    companion object {
        //bcz we want to have only one instance of database and not multiple instances
        //volatile means all threads will have same val

        @Volatile
        private var instance: MoviesDatabase? = null

        //to make sure no 2 threads are currently doing same thing
        private val LOCK = Any()

        operator fun invoke(context: Context) = instance ?: synchronized(LOCK) {
            instance ?: buildDatabase(context).also {
                instance = it
            }
        }

        private fun buildDatabase(context: Context) = Room.databaseBuilder(
            context.applicationContext,
            MoviesDatabase::class.java, "movies.db"
        ).build()
    }
}