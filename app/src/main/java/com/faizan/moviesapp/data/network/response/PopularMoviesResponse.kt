package com.faizan.moviesapp.data.network.response

import com.faizan.moviesapp.data.db.entity.PopularMoviesEntry
import com.google.gson.annotations.SerializedName

data class PopularMoviesResponse(
    val page: Int,
    val results: List<PopularMoviesEntry>,
    @SerializedName("total_pages")
    val totalPages: Int,
    @SerializedName("total_results")
    val totalResults: Int
)