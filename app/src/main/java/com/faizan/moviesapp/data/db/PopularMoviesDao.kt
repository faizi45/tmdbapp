package com.faizan.moviesapp.data.db

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.faizan.moviesapp.data.db.entity.PopularMoviesEntry

@Dao
interface PopularMoviesDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun upsert(popularMoviesEntry: List<PopularMoviesEntry>)

    @Query("Select * from popular_movies")
    fun getPopularMovies(): LiveData<List<PopularMoviesEntry>>
}