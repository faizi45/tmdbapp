package com.faizan.moviesapp.data.network

import android.util.Log
import com.faizan.moviesapp.data.db.entity.MovieDetailResponseDto
import com.faizan.moviesapp.data.network.response.*
import com.jakewharton.retrofit2.adapter.kotlin.coroutines.CoroutineCallAdapterFactory
import kotlinx.coroutines.Deferred
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

const val API_KEY = "e95c5c33d38a6be5f25c91d64c8d80e0"

interface TMDBApiService {

    @GET("discover/movie/")
    fun getPopularMovies(
        @Query("sort_by") name: String
    ): Deferred<Response<PopularMoviesResponse>>

    @GET("discover/tv/")
    fun getPopularTvShows(
        @Query("sort_by") name: String
    ): Deferred<PopularTvShowsResponse>

    @GET("search/movie")
    fun searchMovie(
        @Query("query") query: String
    ): Deferred<SearchMoviesResponse>

    @GET("/genre/movie/list")
    fun getMoviesGenres(

    ): Deferred<MoviesGenresListResponse>

    @GET("movie/{movie_id}")
    fun getMovieDetails(
        @Path("movie_id") movie_id: Int
    ): Deferred<MovieDetailResponseDto>

    @GET("movie/{movie_id}/similar")
    fun getSimilarMovies(
        @Path("movie_id") movie_id: Int
    ): Deferred<SimilarMoviesResponse>

    companion object {

        operator fun invoke(
            connectivityInterceptor: ConnectivityInterceptor
        ): TMDBApiService {

            val requestInterceptorChain = Interceptor { chain ->

                val url = chain.request()
                    .url()
                    .newBuilder()
                    .addQueryParameter("api_key", API_KEY)
                    .build()

                val request = chain.request()
                    .newBuilder()
                    .url(url)
                    .build()

                Log.d("RAW_URL", url.toString())

                return@Interceptor chain.proceed(request)
            }

            val okHttpClient = OkHttpClient.Builder()
                .addInterceptor(requestInterceptorChain)
                .addInterceptor(connectivityInterceptor)
                .build()

            return Retrofit.Builder()
                .client(okHttpClient)
                .baseUrl("https://api.themoviedb.org/3/")
                .addCallAdapterFactory(CoroutineCallAdapterFactory())
                .addConverterFactory(GsonConverterFactory.create())
                .build()
                .create(TMDBApiService::class.java)


        }
    }
}