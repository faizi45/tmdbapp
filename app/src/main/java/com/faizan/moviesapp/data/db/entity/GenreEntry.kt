package com.faizan.moviesapp.data.db.entity

import androidx.room.Entity
import androidx.room.Index
import androidx.room.PrimaryKey

@Entity(tableName = "movies_genres", indices = [Index(value = ["id"], unique = true)])

data class GenreEntry(

    @PrimaryKey(autoGenerate = false)
    val id: Int? = null,
    val name: String
)