package com.faizan.moviesapp.data.repository

import android.util.Log
import androidx.lifecycle.LiveData
import com.faizan.moviesapp.data.db.*
import com.faizan.moviesapp.data.db.entity.*
import com.faizan.moviesapp.data.network.MovieNetworkDataSource
import com.faizan.moviesapp.data.network.Status
import com.faizan.moviesapp.data.network.response.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class TMDBRepositoryImpl(
    private val popularMoviesDao: PopularMoviesDao,
    private val popularTvShowsDao: PopularTvShowsDao,
    private val searchMoveisDao: SearchMoveisDao,
    private val moviesGenresDao: MoviesGenresDao,
    private val movieDetailsDao: MovieDetailsDao,
    private val similarMoviesDao: SimilarMoviesDao,
    private val movieNetworkDataSource: MovieNetworkDataSource
) : TMDBRepository {

    private lateinit var searchMoviesResponse: LiveData<SearchMoviesResponse>

    init {
        movieNetworkDataSource.apply {
            downloadedPopularMoviesResponse.observeForever {

                    newPopularMovies ->
                if (newPopularMovies.status == Status.SUCCESS)
                    persistFetchedPopularMovies(newPopularMovies.data!!)
            }

            downloadedPopularTvShowsResponse.observeForever {

                    newPopularTvShows ->
                persistFetchedTvShows(newPopularTvShows)
            }

            downloadedSearchMoviesResponse.observeForever { searchMovies ->
                searchMoviesResponse = downloadedSearchMoviesResponse
//                getSearchMovies()
            }


            downloadedMoviesGenres.observeForever { moviesGenres ->
                persistMoviesGenres(moviesGenres)
            }

            downloadedMovieDetails.observeForever {

                    movieDetails ->
                persistMovieDetails(movieDetails)
            }

            downloadedSimilarMoviesResponse.observeForever { similarMovies ->
                persistSimlarMovies(similarMovies)
            }
        }
    }

    //region MOVIES GENRES
    override suspend fun getMoviesGenresList(): LiveData<out List<GenreEntry>> {

        return withContext(Dispatchers.IO) {
            initMoviesGenres()
            return@withContext moviesGenresDao.getMoviesGenres()
        }

    }

    private suspend fun initMoviesGenres() {
        fetchMoviesGenres()
    }

    private suspend fun fetchMoviesGenres() {
        movieNetworkDataSource.fetchMoviesGenres()
    }

    private fun persistMoviesGenres(moviesGenresListResponse: MoviesGenresListResponse) {
        GlobalScope.launch(Dispatchers.IO) {
            moviesGenresDao.upsert(moviesGenresListResponse.genreEntries)
        }
    }
    //endregion MOVIES GENRES

    //region POPULARMOVIES
    override suspend fun getPopularMovies(): LiveData<out List<PopularMoviesEntry>> {

        return withContext(Dispatchers.IO) {
            initPopularMoviesData()
            return@withContext popularMoviesDao.getPopularMovies()
        }
    }

    private suspend fun initPopularMoviesData() {
        fetchPopularMovies()
    }

    private fun persistFetchedPopularMovies(popularMoviesResponse: PopularMoviesResponse) {

        GlobalScope.launch(Dispatchers.IO) {
            popularMoviesDao.upsert(popularMoviesResponse.results)
            Log.d("_SEARCH", popularMoviesResponse.results.toString())
        }
    }

    private suspend fun fetchPopularMovies() {

        movieNetworkDataSource.fetchPopularMovies("popularity.desc")
    }

    //endregion POPULARMOVIES

    //region POPULARTVSHOWS

    override suspend fun getPopularTvShows(): LiveData<out List<PopularTvEntry>> {
        return withContext(Dispatchers.IO) {
            initPopularTvShowsDao()
            return@withContext popularTvShowsDao.getPopularTvShows()
        }
    }

    private suspend fun initPopularTvShowsDao() {
        fetchPopularTvShows()
    }

    private suspend fun fetchPopularTvShows() {
        movieNetworkDataSource.fetchPopularTvShows("popularity.asc")
    }

    private fun persistFetchedTvShows(popularTvShowsResponse: PopularTvShowsResponse) {
        GlobalScope.launch(Dispatchers.IO) {
            popularTvShowsDao.insert(popularTvShowsResponse.popularTvEntries)
        }
    }

    //endregion POPULARTVSHOWS

    //region SEARCHMOVIES
    override suspend fun getSearchMoviesResponse(query: String): LiveData<SearchMoviesResponse> {

        getSearchMovies(query)

        if (::searchMoviesResponse.isInitialized)
            return searchMoviesResponse
        else {
            getSearchMovies(query)
            return getSearchMoviesResponse(query)
        }
        /* return withContext(Dispatchers.IO) {
 //            initSearchMovies(query)

             movieNetworkDataSource.searchMovies(query)

             return@withContext searchMoviesResponse.value.results
         }*/
    }

    private suspend fun initSearchMovies(query: String) {
        getSearchMovies(query)
    }

    private suspend fun getSearchMovies(query: String) {

        movieNetworkDataSource.searchMovies(query)
    }

    private fun persistSearchedMovies(searchMoviesResponse: SearchMoviesResponse) {
        GlobalScope.launch(Dispatchers.IO) {
            searchMoveisDao.upsert(searchMoviesResponse.results)
        }
    }
    //endregion SEARCHMOVIES

    //region MOVIE DETAILS
    override suspend fun getMovieDetailsResponse(movieId: Int): LiveData<out MovieDetailResponseDto> {

        return withContext(Dispatchers.IO) {
            initMovieDetails(movieId)
            return@withContext movieDetailsDao.getMovieDetails(movieId)
        }
    }

    private suspend fun initMovieDetails(movieId: Int) {
//        persistMovieDetails(movieDetailResponseDto)
        getMovieDetails(movieId)
    }

    private suspend fun getMovieDetails(movieId: Int) {
        movieNetworkDataSource.fetchMovieDetails(movieId)
    }

    private fun persistMovieDetails(movieDetailResponseDto: MovieDetailResponseDto) {

        GlobalScope.launch(Dispatchers.IO) {
            movieDetailsDao.upsert(movieDetailResponseDto)
        }
    }
    //endregion MOVIE DETAILS

    //region SIMILAR MOVIES

    override suspend fun getSimilarMovies(movieId: Int): LiveData<out List<SimilarMoviesEntry>> {

        return withContext(Dispatchers.IO) {
            initSimilarMovies(movieId)
            return@withContext similarMoviesDao.getSimilarMovies()
        }
    }

    private suspend fun initSimilarMovies(movieId: Int) {
        fetchSimilarMovies(movieId)
    }

    private suspend fun fetchSimilarMovies(movieId: Int) {

        movieNetworkDataSource.fetchSimilarMovies(movieId)
    }

    private fun persistSimlarMovies(similarMoviesResponse: SimilarMoviesResponse) {
        GlobalScope.launch(Dispatchers.IO) {
            similarMoviesDao.upsert(similarMoviesResponse.results)
            Log.d("RAW_RESPONSE", similarMoviesResponse.results.toString())
        }
    }
    //endregion SIMILAR MOVIES
}