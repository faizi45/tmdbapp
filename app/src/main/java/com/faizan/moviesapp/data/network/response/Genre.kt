package com.faizan.moviesapp.data.network.response

data class Genre(
    val id: Int? = null,
    val name: String? = null
)