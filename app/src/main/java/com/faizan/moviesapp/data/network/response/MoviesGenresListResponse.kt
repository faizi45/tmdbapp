package com.faizan.moviesapp.data.network.response

import com.faizan.moviesapp.data.db.entity.GenreEntry
import com.google.gson.annotations.SerializedName

data class MoviesGenresListResponse(
    @SerializedName("genres")
    val genreEntries: List<GenreEntry>
)