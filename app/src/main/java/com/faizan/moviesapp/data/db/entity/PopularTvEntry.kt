package com.faizan.moviesapp.data.db.entity

import androidx.room.Entity
import androidx.room.Index
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName

@Entity(tableName = "popular_tv_shows", indices = [Index(value = ["id"], unique = true)])

data class PopularTvEntry(
    @SerializedName("backdrop_path")
    val backdropPath: String? = null,
    @SerializedName("first_air_date")
    val firstAirDate: String,
//    @SerializedName("genre_ids")
//    val genreIds: List<Int>,
    @PrimaryKey(autoGenerate = false)
    val id: Int? = null,
    val name: String,
//    @SerializedName("origin_country")
//    val originCountry: List<String>,
    @SerializedName("original_language")
    val originalLanguage: String,
    @SerializedName("original_name")
    val originalName: String,
    val overview: String,
    val popularity: Double,
    @SerializedName("poster_path")
    val posterPath: String? = null,
    @SerializedName("vote_average")
    val voteAverage: Double,
    @SerializedName("vote_count")
    val voteCount: Int
)