package com.faizan.moviesapp

import android.app.Application
import com.faizan.moviesapp.data.db.MoviesDatabase
import com.faizan.moviesapp.data.network.*
import com.faizan.moviesapp.data.repository.TMDBRepository
import com.faizan.moviesapp.data.repository.TMDBRepositoryImpl
import com.faizan.moviesapp.internal.InternetUtil
import com.faizan.moviesapp.ui.movies.MovieDetailViewModelFactory
import com.faizan.moviesapp.ui.movies.MoviesListViewModelFactory
import com.faizan.moviesapp.ui.search.SearchViewModelFactory
import com.faizan.moviesapp.ui.tvshows.TvShowsListViewModelFactory
import org.kodein.di.Kodein
import org.kodein.di.KodeinAware
import org.kodein.di.android.x.androidXModule
import org.kodein.di.generic.*

class MoviesApplication : Application(), KodeinAware {

    //kodien for dependency injection so it should be kodinAware

    override val kodein = Kodein.lazy {
        //androidXModule provide us with instances of context, services and anything that is related to android
        //so that we dont need to reinvent the wheel
        import(androidXModule(this@MoviesApplication))


        //first we have to bind our database as it doesnt have any interface so we have to bind its class
        //it should have only single instance of database so it is singleton
        //instance is returned form androidXModule and in this case it is application context
        bind() from singleton { MoviesDatabase(instance()) }

        //then we bind our popular movies dao
        //we need to get database returned from above binding and get that instance to singleton below
        //so this way we have bound movies database to popular movies dao
        //in similar way, we can bind multiple other dao's below
        bind() from singleton { instance<MoviesDatabase>().popularMoviesDao() }

        bind() from singleton { instance<MoviesDatabase>().popularTvShowsDao() }

        bind() from singleton { instance<MoviesDatabase>().searchMoviesDao() }

        bind() from singleton { instance<MoviesDatabase>().moviesGenresDao() }

        bind() from singleton { instance<MoviesDatabase>().movieDetailsDao() }

        bind() from singleton { instance<MoviesDatabase>().similarMoviesDao() }

        //bind connectivity interceptor as well as its underlying implementation
        bind<ConnectivityInterceptor>() with singleton { ConnectivityInterceptorImpl(instance()) }

        //bind tmdb api service
        //tmdb api service needs connectivity interceptor but we are passing instance here
        //because kodein will automatically get the connectivity interceptor from above binding
        bind() from singleton { TMDBApiService(instance()) }

        bind<MovieNetworkDataSource>() with singleton { MovieNetworkDataSourceImpl(instance()) }

        bind<TMDBRepository>() with singleton {
            TMDBRepositoryImpl(
                instance(),
                instance(),
                instance(),
                instance(),
                instance(),
                instance(),
                instance()
            )
        }

        bind() from provider { MoviesListViewModelFactory(instance()) }

        bind() from provider { TvShowsListViewModelFactory(instance()) }

        bind() from provider { SearchViewModelFactory(instance()) }

        bind() from factory { id: Int ->
            MovieDetailViewModelFactory(id, instance())
        }
    }

    override fun onCreate() {
        super.onCreate()
        InternetUtil.init(this)
        //initialize the libs here
        //will set default values for the first time the app launches
    }

}