package com.faizan.moviesapp.ui.movies

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.faizan.moviesapp.data.repository.TMDBRepository

class MoviesListViewModelFactory(
    private val tmdbRepository: TMDBRepository
) : ViewModelProvider.NewInstanceFactory() {

    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return MoviesListViewModel(tmdbRepository) as T
    }

}