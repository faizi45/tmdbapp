package com.faizan.moviesapp.ui.movies

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.GridLayoutManager
import com.faizan.moviesapp.R
import com.faizan.moviesapp.data.db.entity.SimilarMoviesEntry
import com.faizan.moviesapp.internal.glide.GlideApp
import com.faizan.moviesapp.ui.ScopedFragment
import com.faizan.moviesapp.ui.movies.similar.SimilarMoviesItem
import com.xwray.groupie.GroupAdapter
import com.xwray.groupie.kotlinandroidextensions.ViewHolder
import kotlinx.android.synthetic.main.fragment_movies_details.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import org.kodein.di.KodeinAware
import org.kodein.di.android.x.closestKodein
import org.kodein.di.generic.factory

class MoviesDetailFragment : ScopedFragment(), KodeinAware {

    override val kodein by closestKodein()

    private lateinit var viewModel: MovieDetailViewModel

    private val viewModelFactory: ((Int) -> MovieDetailViewModelFactory) by factory()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        return inflater.inflate(R.layout.fragment_movies_details, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        val safeArgs = arguments?.let { MoviesDetailFragmentArgs.fromBundle(it) }
        val id = safeArgs?.movieId ?: throw Exception()

        viewModel = ViewModelProviders.of(this, viewModelFactory(id)).get(MovieDetailViewModel::class.java)

        bindUI()
    }


    override fun onResume() {
        super.onResume()
        shimmer_view_container.startShimmerAnimation()
        shimmer_view_similar_movies.startShimmerAnimation()
    }

    override fun onPause() {
        super.onPause()
        shimmer_view_container.stopShimmerAnimation()
        shimmer_view_similar_movies.stopShimmerAnimation()

    }

    private fun bindUI() = launch(Dispatchers.Main) {
        val movieDetails = viewModel.movieDetails.await()

        movieDetails.observe(this@MoviesDetailFragment, Observer { details ->
            shimmer_view_container.stopShimmerAnimation()
            shimmer_view_container.visibility = View.GONE

            if (details == null) {
                rvSimilarMovies.visibility = View.GONE
                tvEmptyMessage.visibility = View.VISIBLE
                return@Observer
            }

            tvMovieName.text = details.title
            tvMovieDescription.text = details.overview

            GlideApp.with(this@MoviesDetailFragment)
                .load("https://image.tmdb.org/t/p/w780/" + details.posterPath)
                .error(R.drawable.square)
                .placeholder(R.drawable.square)
                .into(ivPoster)

            movieDetails.removeObservers(this@MoviesDetailFragment)
        })

        val similarMovies = viewModel.similarMovies.await()

        similarMovies.observe(this@MoviesDetailFragment, Observer { allSimilarMovies ->

            shimmer_view_similar_movies.stopShimmerAnimation()
            shimmer_view_similar_movies.visibility = View.GONE

            Log.d("RAW_RESPONSE_LOCAL", allSimilarMovies.toString())

            if (allSimilarMovies == null) return@Observer


            initRecyclerView(allSimilarMovies)
        })
    }

    private fun List<SimilarMoviesEntry>.toSimilarMovieItem(): List<SimilarMoviesItem> {

        return this.map {
            SimilarMoviesItem(it)
        }
    }

    private fun initRecyclerView(items: List<SimilarMoviesEntry>) {

        val groupAdapter = GroupAdapter<ViewHolder>().apply {
            addAll(items.toSimilarMovieItem())
        }

        rvSimilarMovies.apply {
            layoutManager = GridLayoutManager(this@MoviesDetailFragment.context, 2)
//            layoutManager = LinearLayoutManager(this@MoviesDetailFragment.context, RecyclerView.VERTICAL, false)
            adapter = groupAdapter
        }

        groupAdapter.setOnItemClickListener { item, view ->

        }
    }
}