package com.faizan.moviesapp.ui.tvshows

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.faizan.moviesapp.data.repository.TMDBRepository

class TvShowsListViewModelFactory(
    private val tmdbRepository: TMDBRepository
) : ViewModelProvider.NewInstanceFactory() {

    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return TvShowsListViewModel(tmdbRepository) as T
    }
}