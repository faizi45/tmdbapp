package com.faizan.moviesapp.ui.search

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.faizan.moviesapp.data.repository.TMDBRepository

class SearchViewModelFactory(
    private val tmdbRepository: TMDBRepository
) : ViewModelProvider.NewInstanceFactory() {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return SearchViewModel(tmdbRepository) as T
    }
}