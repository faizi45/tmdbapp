package com.faizan.moviesapp.ui.tvshows

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.faizan.moviesapp.R
import com.faizan.moviesapp.data.db.entity.PopularTvEntry
import com.faizan.moviesapp.internal.InternetUtil
import com.faizan.moviesapp.ui.ScopedFragment
import com.xwray.groupie.GroupAdapter
import com.xwray.groupie.ViewHolder
import kotlinx.android.synthetic.main.fragment_popular_tvshows.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import org.kodein.di.KodeinAware
import org.kodein.di.android.x.closestKodein
import org.kodein.di.generic.instance

class TvShowsListFragment : ScopedFragment(), KodeinAware {

    override val kodein by closestKodein()

    private val viewModelFactory: TvShowsListViewModelFactory by instance()

    private lateinit var viewModel: TvShowsListViewModel

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_popular_tvshows, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        viewModel = ViewModelProviders.of(this, viewModelFactory).get(TvShowsListViewModel::class.java)

        InternetUtil.observe(this@TvShowsListFragment, Observer { status ->
            if (!status) {
                Log.d("_LIST: status: ", status.toString())
                Toast.makeText(this@TvShowsListFragment.context, "No internet", Toast.LENGTH_SHORT).show()
                bindUI()
            } else {
                Log.d("_LIST: status: ", status.toString())
                Toast.makeText(this@TvShowsListFragment.context, "Now connected", Toast.LENGTH_SHORT).show()
                bindUI()
            }
        })

    }

    override fun onResume() {
        super.onResume()
        shimmer_view_container.startShimmerAnimation()
    }

    override fun onPause() {
        super.onPause()
        shimmer_view_container.stopShimmerAnimation()
    }

    private fun bindUI() = launch {
        Dispatchers.Main
        val popularTvShowsEntries = viewModel.popularTvShowsList.await()


        popularTvShowsEntries.observe(this@TvShowsListFragment, Observer { popularTvShowsList ->
            shimmer_view_container.stopShimmerAnimation()
            shimmer_view_container.visibility = View.GONE

            if (popularTvShowsList == null) return@Observer

            initRecyclerView(popularTvShowsList)
        })
    }

    private fun initRecyclerView(items: List<PopularTvEntry>) {
        val groupAdapter = GroupAdapter<ViewHolder>().apply {
            addAll(items.toPopularTvShowItem())
        }

        recyclerView.apply {
            layoutManager = LinearLayoutManager(this@TvShowsListFragment.context)
            adapter = groupAdapter
        }
    }

    private fun List<PopularTvEntry>.toPopularTvShowItem(): List<PopularTvShowsItem> {
        return this.map { PopularTvShowsItem(it) }
    }


}