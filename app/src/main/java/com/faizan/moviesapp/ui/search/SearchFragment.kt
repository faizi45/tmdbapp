package com.faizan.moviesapp.ui.search

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.faizan.moviesapp.R
import com.faizan.moviesapp.data.network.response.SearchMovieEntry
import com.faizan.moviesapp.databinding.FragmentSearchBinding
import com.faizan.moviesapp.ui.ScopedFragment
import com.xwray.groupie.GroupAdapter
import com.xwray.groupie.ViewHolder
import kotlinx.android.synthetic.main.fragment_search.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import org.kodein.di.KodeinAware
import org.kodein.di.android.x.closestKodein
import org.kodein.di.generic.instance

class SearchFragment : ScopedFragment(), KodeinAware {

    override val kodein by closestKodein()

    private val viewModelFactory: SearchViewModelFactory by instance()

    private lateinit var viewModel: SearchViewModel
    private lateinit var binding: FragmentSearchBinding

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_search, container, false)
        return binding.root
//        return inflater.inflate(R.layout.fragment_search, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        viewModel = ViewModelProviders.of(this, viewModelFactory).get(SearchViewModel::class.java)

        binding.viewmodel = viewModel

        bindUI()
    }

    private fun bindUI() = launch {
        Dispatchers.Main
        val popularMoviesEntries = viewModel.data

        if (popularMoviesEntries == null) {
            Log.d("SEARCH_FRAG", "entries data null")
        } else {
            Log.d("SEARCH_FRAG", "entries data not null")
//            Log.d("SEARCH_FRAG", "total pages :" + popularMoviesEntries.value!!.totalPages)

        }

        popularMoviesEntries.observe(this@SearchFragment, Observer { searchedMoviesList ->
            if (searchedMoviesList == null) {

                return@Observer
            }


            Log.d("_SEARCH", searchedMoviesList.toString())
            initRecyclerView(searchedMoviesList.results)

            popularMoviesEntries.removeObservers(this@SearchFragment)
        })
    }

    private fun List<SearchMovieEntry>.toPopularMovieItem(): List<SearchMovieItem> {

        return this.map {
            SearchMovieItem(it)
        }
    }

    private fun initRecyclerView(items: List<SearchMovieEntry>) {


        val groupAdapter = GroupAdapter<ViewHolder>().apply {
            this.clear()
            this.notifyDataSetChanged()

            addAll(items.toPopularMovieItem())

            viewModel.data.removeObservers(this@SearchFragment)

        }

        rvSearch.apply {
            layoutManager = LinearLayoutManager(this@SearchFragment.context, RecyclerView.VERTICAL, false)
            adapter = groupAdapter
        }

        groupAdapter.setOnItemClickListener { item, view ->

            //            Toast.makeText(this@MoviesListFragment.context, "Clicked", Toast.LENGTH_SHORT).show()
        }
    }

}