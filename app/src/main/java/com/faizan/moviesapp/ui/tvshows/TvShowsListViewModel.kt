package com.faizan.moviesapp.ui.tvshows

import androidx.lifecycle.ViewModel
import com.faizan.moviesapp.data.repository.TMDBRepository
import com.faizan.moviesapp.internal.lazyDeferred

class TvShowsListViewModel(
    private val tmdbRepository: TMDBRepository
) : ViewModel() {

    val popularTvShowsList by lazyDeferred {
        tmdbRepository.getPopularTvShows()
    }
}