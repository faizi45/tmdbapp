package com.faizan.moviesapp.ui.movies

import androidx.lifecycle.ViewModel
import com.faizan.moviesapp.data.repository.TMDBRepository
import com.faizan.moviesapp.internal.lazyDeferred

class MovieDetailViewModel(
    private val movieId: Int,
    private val tmdbRepository: TMDBRepository
) : ViewModel() {

    val movieDetails by lazyDeferred {
        tmdbRepository.getMovieDetailsResponse(movieId)
    }

    val similarMovies by lazyDeferred {
        tmdbRepository.getSimilarMovies(movieId)
    }
}