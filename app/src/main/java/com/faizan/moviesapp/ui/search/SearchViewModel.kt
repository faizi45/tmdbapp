package com.faizan.moviesapp.ui.search

import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.faizan.moviesapp.data.network.response.SearchMoviesResponse
import com.faizan.moviesapp.data.repository.TMDBRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

class SearchViewModel(private val tmdbRepository: TMDBRepository) :
    ViewModel() {

    var searchQuery: String? = null
    /*
    *TODO
    *implement search query here
     */
    val data = MutableLiveData<SearchMoviesResponse>()


    fun onSearchQuery(view: View) {

        data.postValue(null)

        Log.d("SEARCHVM", searchQuery)

        GlobalScope.launch(Dispatchers.IO) {


            var r = tmdbRepository.getSearchMoviesResponse(searchQuery!!)

            data.postValue(r.value)

//            searchMoveisDao.upsert(searchMoviesResponse.results)
        }
        Toast.makeText(view.context, searchQuery, Toast.LENGTH_SHORT).show()

        if (data.value != null)
            Log.d("SEARCHVM", data.value!!.results.toString())
    }

//    val searchMovies by lazyDeferred {
//        tmdbRepository.getSearchMoviesResponse("batman")
//    }
}