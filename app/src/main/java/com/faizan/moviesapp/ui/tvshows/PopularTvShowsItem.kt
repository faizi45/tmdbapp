package com.faizan.moviesapp.ui.tvshows

import com.faizan.moviesapp.R
import com.faizan.moviesapp.data.db.entity.PopularTvEntry
import com.faizan.moviesapp.internal.glide.GlideApp
import com.xwray.groupie.kotlinandroidextensions.Item
import com.xwray.groupie.kotlinandroidextensions.ViewHolder
import kotlinx.android.synthetic.main.item_popular_movies.*

class PopularTvShowsItem(
    val popularTvEntry: PopularTvEntry
) : Item() {
    override fun bind(viewHolder: ViewHolder, position: Int) {
        viewHolder.apply {
            tvMovieName.text = popularTvEntry.originalName
            if (popularTvEntry.posterPath != null)
                updateMovieImage()

        }

    }


    override fun getLayout() = R.layout.item_popular_movies

    private fun ViewHolder.updateMovieImage() {
        GlideApp.with(this.containerView)
            .load("https://image.tmdb.org/t/p/w500/" + popularTvEntry.posterPath)
            .error(R.drawable.square)
            .into(ivMoviePoster)

    }
}