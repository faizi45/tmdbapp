package com.faizan.moviesapp.ui.movies.similar

import com.faizan.moviesapp.R
import com.faizan.moviesapp.data.db.entity.SimilarMoviesEntry
import com.faizan.moviesapp.internal.glide.GlideApp
import com.xwray.groupie.kotlinandroidextensions.Item
import com.xwray.groupie.kotlinandroidextensions.ViewHolder
import kotlinx.android.synthetic.main.item_similar_movies.*

class SimilarMoviesItem(
    val similarMoviesEntry: SimilarMoviesEntry
) : Item() {

    override fun bind(viewHolder: ViewHolder, position: Int) {
        viewHolder.apply {
            tvMovieName.text = similarMoviesEntry.originalTitle


            if (similarMoviesEntry.posterPath != null)
                updateMovieImage()
        }
    }

    override fun getLayout() = R.layout.item_similar_movies

    private fun ViewHolder.updateMovieImage() {
        GlideApp.with(this.containerView)
            .load("https://image.tmdb.org/t/p/w500/" + similarMoviesEntry.posterPath)
            .error(R.drawable.square)
            .into(ivMoviePoster)

    }
}