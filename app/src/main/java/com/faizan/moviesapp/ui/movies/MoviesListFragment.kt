package com.faizan.moviesapp.ui.movies

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.View.GONE
import android.view.ViewGroup
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.Navigation
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.faizan.moviesapp.R
import com.faizan.moviesapp.data.db.entity.PopularMoviesEntry
import com.faizan.moviesapp.internal.InternetUtil
import com.faizan.moviesapp.ui.ScopedFragment
import com.xwray.groupie.GroupAdapter
import com.xwray.groupie.ViewHolder
import kotlinx.android.synthetic.main.fragment_popular_movies.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import org.kodein.di.KodeinAware
import org.kodein.di.android.x.closestKodein
import org.kodein.di.generic.instance

class MoviesListFragment : ScopedFragment(), KodeinAware {

    override val kodein by closestKodein()
    private val viewModelFactory: MoviesListViewModelFactory by instance()

    private lateinit var viewModel: MoviesListViewModel

    private var isConnected: Boolean = false

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        return inflater.inflate(R.layout.fragment_popular_movies, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        viewModel = ViewModelProviders.of(this, viewModelFactory).get(MoviesListViewModel::class.java)
        InternetUtil.observe(this@MoviesListFragment, Observer { status ->
            if (!status) {
                Log.d("_LIST: status: ", status.toString())
                Toast.makeText(this@MoviesListFragment.context, "No internet", Toast.LENGTH_SHORT).show()
                bindUI()
            } else {
                Log.d("_LIST: status: ", status.toString())
                Toast.makeText(this@MoviesListFragment.context, "Now connected", Toast.LENGTH_SHORT).show()
                bindUI()
            }

//            bindUI()
        })

//        bindUI()
    }

    override fun onResume() {
        super.onResume()
        shimmer_view_container.startShimmerAnimation()
    }

    override fun onPause() {
        super.onPause()
        shimmer_view_container.stopShimmerAnimation()
    }

    private fun bindUI() = launch {
        Log.d("_LIST: bindUI(): ", "*")
        Dispatchers.Main
        val popularMoviesEntries = viewModel.popularMoviesList.await()

        popularMoviesEntries.observe(this@MoviesListFragment, Observer { popularMoviesList ->
            shimmer_view_container.stopShimmerAnimation()
            shimmer_view_container.visibility = GONE


/*            InternetUtil.observe(this@MoviesListFragment, Observer { status ->
                if (!status)
                    Toast.makeText(this@MoviesListFragment.context, "No internet", Toast.LENGTH_SHORT).show()
                else {
                    Toast.makeText(this@MoviesListFragment.context, "Now connected", Toast.LENGTH_SHORT).show()
                }
            })*/

            if (popularMoviesList == null) {
                Log.d("_LIST: bindUI(): ", "LIST IS NULL")
                return@Observer
            }

            Log.d("_LIST: bindUI(): ", popularMoviesList.size.toString())

            initRecyclerView(popularMoviesList)

        })
    }

    private fun List<PopularMoviesEntry>.toPopularMovieItem(): List<PopularMoviesItem> {

        return this.map {
            PopularMoviesItem(it)
        }
    }

    private fun initRecyclerView(items: List<PopularMoviesEntry>) {

        val groupAdapter = GroupAdapter<ViewHolder>().apply {
            addAll(items.toPopularMovieItem())
        }

        recyclerView.apply {
            layoutManager = LinearLayoutManager(this@MoviesListFragment.context, RecyclerView.VERTICAL, false)
            adapter = groupAdapter
        }

        groupAdapter.setOnItemClickListener { item, view ->

            (item as? PopularMoviesItem)?.let {
                showMoviesDetails(it.poularMoviesEntry.id!!, view)
            }

//            Toast.makeText(this@MoviesListFragment.context, "Clicked", Toast.LENGTH_SHORT).show()
        }
    }

    private fun showMoviesDetails(id: Int, view: View) {
        val actionDetail = MoviesListFragmentDirections.actionDetail(id)

        Navigation.findNavController(view).navigate(actionDetail)

    }
}