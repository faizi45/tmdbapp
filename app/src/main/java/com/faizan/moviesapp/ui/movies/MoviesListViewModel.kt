package com.faizan.moviesapp.ui.movies

import androidx.lifecycle.ViewModel
import com.faizan.moviesapp.data.repository.TMDBRepository
import com.faizan.moviesapp.internal.lazyDeferred

class MoviesListViewModel(
    private val tmdbRepository: TMDBRepository
) : ViewModel() {

    val popularMoviesList by lazyDeferred {
        tmdbRepository.getPopularMovies()
    }

}