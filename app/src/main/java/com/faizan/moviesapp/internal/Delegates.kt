package com.faizan.moviesapp.internal

import kotlinx.coroutines.*

fun <T> lazyDeferred(block: suspend CoroutineScope.() -> T): Lazy<Deferred<T>> {

    return lazy {
        //means coroutine will be launched only when lazyDeferred is called
        //lazyDeferred will be launched when we need the weather in viewmodel
        //and this chain will be initiated when weather val is null for first time
        GlobalScope.async(start = CoroutineStart.LAZY) {
            block.invoke(this)
        }
    }
}