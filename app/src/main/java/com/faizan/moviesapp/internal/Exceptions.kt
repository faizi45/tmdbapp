package com.faizan.moviesapp.internal

import java.io.IOException

class NoConnectivityException() : IOException()